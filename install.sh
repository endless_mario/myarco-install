#!/bin/bash

# My install script to modify Arco after a fresh install
# By Mario Spörl

packages='keepassxc
          libreoffice-fresh
          atom
          hunspell-de
          virtualbox
          virtualbox-guest-iso
          virtualbox-host-dkms'

for package in $packages
do
    echo "########################################"
    echo "$package"
    echo "########################################"
    echo

    sudo pacman -S --noconfirm --needed $package

    echo
    echo "########################################"
    echo "Done $package"
    echo "########################################"
    echo
done

# All packages from the AUR

packagesAur='virtualbox-ext-oracle
             meld-git
             gtg-git'

for packageAur in $packagesAur
do
    echo "########################################"
    echo "$packageAur"
    echo "########################################"
    echo

    paru -S --noconfirm --needed $packageAur

    echo "########################################"
    echo "$packageAur"
    echo "########################################"
    echo
done

echo "########################################"
echo "All done"
echo "########################################"
